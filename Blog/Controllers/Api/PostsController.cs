﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Blog.Models;
using Microsoft.ApplicationInsights.WindowsServer;

namespace Blog.Controllers.Api
{
   

    public class PostsController : ApiController
    {
        private readonly ApplicationDbContext _contex;

        public PostsController(ApplicationDbContext contex)
        {
            _contex = contex;
        }

        //Get /api/posts
        public async Task<IHttpActionResult> GetPosts()
        {
            var posts = await _contex.Posts.ToListAsync();

            return Ok(posts);
        }

        //Get /api/posts/1
        public async Task<IHttpActionResult>  GetPost(int id)
        {
            var post = await _contex.Posts.SingleOrDefaultAsync();

            if (post == null)
                return NotFound();

            return Ok(post);
        }

        //Post /api/posts
        [HttpPost]
        public async Task<IHttpActionResult> CreatePost(Post post)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _contex.Posts.Add(post);
            _contex.SaveChanges();

            return Ok(post);

        }

        //PUT /api/post/1
        [HttpPut]
        public async Task<IHttpActionResult> UpdatePost(int id,Post post)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var postInDb = await _contex.Posts.SingleOrDefaultAsync(p => p.Id == id);

            if (postInDb == null)
                return NotFound();

            postInDb.Name = post.Name;
            postInDb.Content = post.Content;
            postInDb.Author = post.Author;
            postInDb.CategoryId = post.CategoryId;
            postInDb.Tags = post.Tags;
            postInDb.ReleasedDate = post.ReleasedDate;

            _contex.SaveChanges();

            return Ok(post);

        }       

        //DELETE /api/post/1
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCustomer(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var post =  await _contex.Posts.SingleOrDefaultAsync(p => p.Id == id);

            if (post == null)
                return NotFound();

            _contex.Posts.Remove(post);
            _contex.SaveChanges();

            return Ok();
        }

    }
}
